package com.solomia;

import com.solomia.entities.Author;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import java.util.Iterator;
import java.util.List;

import static io.restassured.RestAssured.given;

//import static io.restassured.RestAssured.get;

public class Application {



    public static void main(String[] args) {
//        Response response = get("api/library/authors");
//        List<Author> authors = response.jsonPath().getList("$", Author.class);
//        authors.forEach(System.out::println);



//        Author author1 = new Author(5, new Author.Name("Solomia", "Lylyk"), "ukrainian", new Author.Birth("31.10.1999", "Ukraine", "Lviv"), "Student and that's all");
//        Author author = new Author();
//        long id =60;
//        author.setAuthorId(60);
//        author.setAuthorName(new Author.Name("Solomia", "lylyk"));
//        Response response = given().body(author1).when().contentType(ContentType.JSON).put(URL.BASE_URL+ URL.GET_AUTHOR_BY_ID+ 5);
//        int status = response.getStatusCode();
//        System.out.println(status);

        RequestSpecification requestSpecification = given();
        Response response= requestSpecification.queryParam("orderType", "desc").queryParam("size", "10").get("api/library/authors");
        List<Author> authors = response.jsonPath().getList("$", Author.class);
        Iterator<Author> authorIterator = authors.iterator();
        authors.forEach(System.out::println);

//        while (authorIterator.hasNext()) {
//            Author current = authorIterator.next();
//            System.out.println("Current" + current);
//            if(current.getAuthorId()>authorIterator.next().getAuthorId()) {
//                System.out.println("+");
//            }
//        }

        for(int i= 0; i< authors.size()-1; i++) {
            System.out.println(authors.get(i));
            if(authors.get(i).getAuthorId()>authors.get(++i).getAuthorId()) {
                System.out.println("+");
            }
            i--;
        }


//        JSONObject requestParams = new JSONObject();
//        requestParams.put("nationality", "ukrainian");
//        requestSpecification.header("Content-Type", "application/json");
//        requestSpecification.body(requestParams.toString());
//        Response response1 = requestSpecification.put(URL.BASE_URL+ URL.GET_AUTHOR_BY_ID+5);
//        System.out.println(response1.getStatusCode());



//        Response response1 = put("api/library/author/"+id);
//        response1.body().



//        RequestSpecification requestSpecification = given().baseUri(URL.BASE_URL).contentType("application/json;charset=UTF-8");
//        Response response = requestSpecification.get("api/library/genres");
//        Genre author = response.jsonPath().getObject("$", Genre.class);
//        System.out.println(author);

//        Response response1 = get("api/library/genres");
//        List<Genre> genres = response1.jsonPath().getList("$", Genre.class);
//        genres.forEach(System.out::println);
    }


}

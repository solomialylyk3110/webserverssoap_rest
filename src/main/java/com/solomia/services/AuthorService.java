package com.solomia.services;

import com.solomia.constants.URL;
import com.solomia.entities.Entity;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

import static io.restassured.RestAssured.*;

public class AuthorService implements Service {


    @Override
    public Response update(Entity author) {
        return given().body(author).when().contentType(ContentType.JSON).put(URL.BASE_URL+ URL.GET_AUTHOR_BY_ID);
    }

    @Override
    public Response getById(long id) {
        return get(URL.BASE_URL+ URL.GET_AUTHOR_BY_ID+id);
    }

    @Override
    public Response create(Entity author) {
        return given().body(author).when().contentType(ContentType.JSON).post(URL.BASE_URL+ URL.GET_AUTHOR_BY_ID);
    }

    @Override
    public Response deleteById(long id) {
        return delete(URL.BASE_URL+ URL.GET_AUTHOR_BY_ID+id);
    }
}

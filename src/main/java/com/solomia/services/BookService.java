package com.solomia.services;

import com.solomia.constants.URL;
import com.solomia.entities.Entity;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

import static io.restassured.RestAssured.*;

public class BookService implements Service {
    public Response update(Entity book) {
        return given().body(book).when().contentType(ContentType.JSON).put(URL.BASE_URL + URL.GET_BOOK_BY_ID);
    }

    public Response getById(long id) {
        return get(URL.BASE_URL + URL.GET_BOOK_BY_ID + id);
    }

    public Response create(Entity book) {
        return given().body(book).when().contentType(ContentType.JSON).post(URL.BASE_URL + URL.GET_BOOK_BY_ID);
    }

    public Response deleteById(long id) {
        return delete(URL.BASE_URL + URL.GET_BOOK_BY_ID + id);
    }
}

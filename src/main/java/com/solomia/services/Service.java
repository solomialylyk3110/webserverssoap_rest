package com.solomia.services;

import com.solomia.entities.Entity;

public interface Service {

    io.restassured.response.Response getById(long id);
    io.restassured.response.Response create(Entity entity);
    io.restassured.response.Response update(Entity entity);
    io.restassured.response.Response deleteById(long id);
}

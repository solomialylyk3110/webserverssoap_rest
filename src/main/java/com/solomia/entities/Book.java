package com.solomia.entities;

public class Book implements Entity{
    private long bookId;
    private String bookName;
    private String bookLanguage;
    private String bookDescription;
    private Additional additional;

    public Book() {
    }

    public static class Additional {
        private long pageCount;
        private Size size;



        public static class Size {
            private long height;
            private long width;
            private long length;

            public Size() {
            }

            public Size(long height, long width, long length) {
                this.height = height;
                this.width = width;
                this.length = length;
            }

            @Override
            public String toString() {
                return "Size{" +
                        "height=" + height +
                        ", width=" + width +
                        ", length=" + length +
                        '}';
            }
        }

        public Additional() {
        }

        public Additional(long pageCount, Size size) {
            this.pageCount = pageCount;
            this.size = size;
        }

        @Override
        public String toString() {
            return "Additional{" +
                    "pageCount=" + pageCount +
                    ", size=" + size +
                    '}';
        }
    }

    private String publicationYear;

    public Book(long bookId, String bookName, String bookLanguage, String bookDescription, Additional additional, String publicationYear) {
        this.bookId = bookId;
        this.bookName = bookName;
        this.bookLanguage = bookLanguage;
        this.bookDescription = bookDescription;
        this.additional = additional;
        this.publicationYear = publicationYear;
    }

    public long getBookId() {
        return bookId;
    }

    public void setBookId(long bookId) {
        this.bookId = bookId;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public String getBookLanguage() {
        return bookLanguage;
    }

    public void setBookLanguage(String bookLanguage) {
        this.bookLanguage = bookLanguage;
    }

    public String getBookDescription() {
        return bookDescription;
    }

    public void setBookDescription(String bookDescription) {
        this.bookDescription = bookDescription;
    }

    public Additional getAdditional() {
        return additional;
    }

    public void setAdditional(Additional additional) {
        this.additional = additional;
    }

    public String getPublicationYear() {
        return publicationYear;
    }

    public void setPublicationYear(String publicationYear) {
        this.publicationYear = publicationYear;
    }

    @Override
    public String toString() {
        return "Book{" +
                "bookId=" + bookId +
                ", bookName='" + bookName + '\'' +
                ", bookLanguage='" + bookLanguage + '\'' +
                ", bookDescription='" + bookDescription + '\'' +
                ", additional=" + additional +
                ", publicationYear='" + publicationYear + '\'' +
                '}';
    }
}

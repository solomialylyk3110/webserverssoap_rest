package com.solomia.entities;

public class Author implements Entity{
    private long authorId;
    private Name authorName;

    public static class Name {
        public String first;
        public String second;

        public Name() {
        }

        public Name(String first, String second) {
            this.first = first;
            this.second = second;
        }

        @Override
        public String toString() {
            return "Name{" +
                    "first='" + first + '\'' +
                    ", second='" + second + '\'' +
                    '}';
        }
    }

    private String nationality;
    private Birth birth;
    public static class Birth {
        public String date;
        public String country;
        public String city;

        public Birth() {
        }

        public Birth(String date, String country, String city) {
            this.date = date;
            this.country = country;
            this.city = city;
        }

        @Override
        public String toString() {
            return "Birth{" +
                    "date='" + date + '\'' +
                    ", country='" + country + '\'' +
                    ", city='" + city + '\'' +
                    '}';
        }
    }

    private String authorDescription;

    public Author() {
    }

    public Author(long authorId, Name authorName, String nationality, Birth birth, String authorDescription) {
        this.authorId = authorId;
        this.authorName = authorName;
        this.nationality = nationality;
        this.birth = birth;
        this.authorDescription = authorDescription;
    }

    public long getAuthorId() {
        return authorId;
    }

    public Name getAuthorName() {
        return authorName;
    }

    public String getNationality() {
        return nationality;
    }

    public Birth getBirth() {
        return birth;
    }

    public String getAuthorDescription() {
        return authorDescription;
    }

    public void setAuthorId(long authorId) {
        this.authorId = authorId;
    }

    public void setAuthorName(Name authorName) {
        this.authorName = authorName;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public void setBirth(Birth birth) {
        this.birth = birth;
    }

    public void setAuthorDescription(String authorDescription) {
        this.authorDescription = authorDescription;
    }

    @Override
    public String toString() {
        return "Author{" +
                "authorID=" + authorId +
                ", authorName=" + authorName +
                ", nationality='" + nationality + '\'' +
                ", birth=" + birth +
                ", authorDescription='" + authorDescription + '\'' +
                '}';
    }
}

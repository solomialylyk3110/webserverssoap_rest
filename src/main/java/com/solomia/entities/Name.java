package com.solomia.entities;

@Deprecated
public class Name {
    private String first;
    private String second;

    public Name() {
    }

    public Name(String first, String second) {
        this.first = first;
        this.second = second;
    }

    @Override
    public String toString() {
        return "Name{" +
                "first='" + first + '\'' +
                ", second='" + second + '\'' +
                '}';
    }
}

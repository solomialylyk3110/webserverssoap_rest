package com.solomia.constants;

public final class URL {
    public static final String BASE_URL = "http://localhost:8080/api/library/";
    public static final String GET_AUTHOR_BY_ID = "author/";
    public static final String GET_BOOK_BY_ID = "book/";
    public static final String GET_GENRE_BY_ID = "genre/";
    public static final String GET_ALL_AUTHORS = "authors";
    public static final String GET_ALL_GENRES = "genres";
    public static final String GET_ALL_BOOKS = "books";
}

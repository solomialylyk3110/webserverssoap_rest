package com.solomia.soapClient;

import javax.xml.soap.*;

@Deprecated
public class AuthorClient {
    public static void main(String[] args) throws SOAPException {
        SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
        SOAPConnection soapConnection = soapConnectionFactory.createConnection();
        MessageFactory messageFactory = MessageFactory.newInstance();
        SOAPMessage soapMessage = messageFactory.createMessage();

        SOAPPart soapPart = soapMessage.getSOAPPart();

        final String libraryNamespace ="lib";
        final String libraryNamespaceURI = "libraryService";

        SOAPEnvelope envelope = soapPart.getEnvelope();
        envelope.addNamespaceDeclaration(libraryNamespace, libraryNamespaceURI);

        SOAPBody body = envelope.getBody();
        SOAPElement getAuthorRequestElement = body.addChildElement("getAuthorRequest", libraryNamespace);
        SOAPElement searchElement = getAuthorRequestElement.addChildElement("search", libraryNamespace);
        SOAPElement sortOrderElement = searchElement.addChildElement("orderType", libraryNamespace);
        sortOrderElement.addTextNode("asc");
        SOAPElement pageElement = searchElement.addChildElement("page", libraryNamespace);
        pageElement.addTextNode("1");
        SOAPElement paginationElement = searchElement.addChildElement("pagination", libraryNamespace);
        paginationElement.addTextNode("true");
        SOAPElement sizeElement = searchElement.addChildElement("size", libraryNamespace);
        sizeElement.addTextNode("10");

        soapMessage.saveChanges();

        String soapEndpointUrl = "libraryService";
        SOAPMessage soapResponse = soapConnection.call(soapMessage, "http://localhost:8080/ws/");
        soapConnection.close();

    }
}

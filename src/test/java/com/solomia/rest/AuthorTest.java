package com.solomia.rest;

import com.solomia.constants.URL;
import com.solomia.entities.Author;
import com.solomia.entities.Entity;
import com.solomia.services.AuthorService;
import com.solomia.services.Service;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.apache.http.HttpStatus;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

import static io.restassured.RestAssured.given;

public class AuthorTest {
    private static Logger logger = LogManager.getLogger(AuthorTest.class);
    private Response response;
    private Service authorService = new AuthorService();

    @Test()
    public void authorExist() {
        logger.info("Test if authorExist");
        long id =5;
        response = authorService.getById(id);
        int status = response.getStatusCode();
        logger.info("Status Code: "+status);
        Assert.assertNotEquals(status, HttpStatus.SC_NOT_FOUND);
        logger.info(response.jsonPath().getObject("$", Author.class));
    }

    @Test
    public void checkQueryParamOrderType() {
        logger.info("Test check query string. OrderType");
        Boolean correctOrder = true;
        String orderType ="desc";
        RequestSpecification requestSpecification = given();
        logger.info("Get response");
        Response response= requestSpecification.queryParam("orderType", orderType).queryParam("size", "50").get(URL.BASE_URL+ URL.GET_ALL_AUTHORS);
        int status = response.getStatusCode();
        logger.info("Status Code: "+status);
        Assert.assertEquals(status, HttpStatus.SC_NOT_FOUND);
        List<Author> authors = response.jsonPath().getList("$", Author.class);
        logger.info("Check order");
        if (orderType.equals("desc")) {
            for(int i= 0; i< authors.size()-1; i++) {
                if(authors.get(i).getAuthorId()<authors.get(++i).getAuthorId()) {
                    correctOrder=false;
                }
                i--;
            }
        } else if (orderType.equals("asc")) {
            for(int i= 0; i< authors.size()-1; i++) {
                if(authors.get(i).getAuthorId()>authors.get(++i).getAuthorId()) {
                    correctOrder = false;
                }
                i--;
            }
        } else {
            correctOrder=false;
            logger.error("Incorrect input of orderType");
        }
        Assert.assertEquals(correctOrder, true);
    }

    @Test
    public void authorGetAll() {
        long id =5;
        logger.info("Test authorGetAll");
        response = authorService.getById(id); //get(URL.BASE_URL+ URL.GET_ALL_AUTHORS); //"api/library/authors"
        int status = response.getStatusCode();
        List<Author> authors = response.jsonPath().getList("$", Author.class);
        System.out.println(authors);
        Assert.assertEquals(status, HttpStatus.SC_OK);
    }

    @Test
    public void updateAuthor() {
        long id = 60;
        authorExist();
        logger.info("Test updateAuthor");
        Entity author = new Author();
        ((Author) author).setAuthorName(new Author.Name("Solomia", "lylyk"));
        response = authorService.update(author);
        int status = response.getStatusCode();
        logger.info(status);
        Assert.assertEquals(status, HttpStatus.SC_OK);
    }
}

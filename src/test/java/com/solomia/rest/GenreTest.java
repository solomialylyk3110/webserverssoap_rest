package com.solomia.rest;

import com.solomia.constants.URL;
import com.solomia.entities.Genre;
import com.solomia.services.GenreService;
import com.solomia.services.Service;
import io.restassured.response.Response;
import org.apache.http.HttpStatus;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

import static io.restassured.RestAssured.get;

public class GenreTest {
    private static Logger logger = LogManager.getLogger(GenreTest.class);
    private Response response;
    private Service genreService = new GenreService();

    @Test()
    public void authorExist() {
        logger.info("Test if genreExist");
        long id =5;
        response = genreService.getById(id);
        logger.info(response.jsonPath().getObject("$", Genre.class));
        int status = response.getStatusCode();
        logger.info("Status Code: "+status);
        Assert.assertNotEquals(status, HttpStatus.SC_NOT_FOUND);
    }

    @Test
    public void genreGetAll() {
        logger.info("Test genreGetAll");
        Response response = get(URL.BASE_URL+ URL.GET_ALL_BOOKS); //"api/library/authors"
        List<Genre> genres = response.jsonPath().getList("$", Genre.class);
        System.out.println(genres);
        int status = response.getStatusCode();
        Assert.assertEquals(status, HttpStatus.SC_OK);
    }

    @Test
    public void updateGenre() {
        //long id = 60;
        authorExist();
        logger.info("Test updateGenre");
        Genre genre = new Genre();
        genre.setGenreName("----");
        response = genreService.update(genre);
        int status = response.getStatusCode();
        logger.info(status);
        Assert.assertEquals(status, HttpStatus.SC_OK);
    }
}

package com.solomia.rest;

import com.solomia.constants.URL;
import com.solomia.entities.Book;
import com.solomia.services.BookService;
import com.solomia.services.Service;
import io.restassured.response.Response;
import org.apache.http.HttpStatus;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

import static io.restassured.RestAssured.get;

public class BookTest {
    private static Logger logger = LogManager.getLogger(BookTest.class);
    private Response response;
    private Service bookService = new BookService();

    @Test()
    public void authorExist() {
        logger.info("Test if bookExist");
        long id =5;
        response = bookService.getById(id);
        int status = response.getStatusCode();
        logger.info("Status Code: "+status);
        logger.info(response.jsonPath().getObject("$", Book.class));
        Assert.assertNotEquals(status, HttpStatus.SC_NOT_FOUND);
    }

    @Test
    public void bookGetAll() {
        logger.info("Test bookGetAll");
        response = get(URL.BASE_URL+ URL.GET_ALL_BOOKS); //"api/library/authors"
        int status = response.getStatusCode();
        Assert.assertEquals(status, HttpStatus.SC_OK);
        List<Book> books = response.jsonPath().getList("$", Book.class);
        System.out.println(books);
    }

    @Test
    public void updateBook() {
        long id = 60;
        authorExist();
        logger.info("Test updateBook");
        Book book = new Book();
        book.setBookName("Fighting club");
        response = bookService.update(book);
        int status = response.getStatusCode();
        logger.info(status);
        Assert.assertEquals(status, HttpStatus.SC_OK);
    }
}
